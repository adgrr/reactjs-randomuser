import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './components/App';
import './styles/styles.scss';

const ReactRoot = document.getElementById('root');

axios
    .get('https://api.randomuser.me')
    .then(function(res) {
      // console.log(res.data.results[0]);
      const {data} = res;
      ReactDOM.render(<App data={data}/>, ReactRoot);
    })
    .catch(function(error) {
      console.log(error);
    });

module.hot.accept();
